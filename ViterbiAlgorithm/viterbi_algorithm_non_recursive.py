# First set of values
observation_sequence = ('Normal', 'Cold', 'Dizzy')
states_set = ('Healthy', 'Fever')
start_probs = {
    'Healthy' : 0.6,
    'Fever' : 0.4
}
transmission_probs = {
    'Healthy' : {'Healthy' : 0.7, 'Fever' : 0.3},
    'Fever' : {'Healthy' : 0.4, 'Fever' : 0.6}
}
emission_probs = {
    'Healthy' : {'Normal' : 0.5, 'Cold' : 0.4, 'Dizzy' : 0.1},
    'Fever' : {'Normal' : 0.1, 'Cold': 0.3, 'Dizzy' : 0.6}
}

# Second set of values
states_set_1 = ('Exon', 'Donor', 'Intron')
observation_sequence_1 = []
for char in "CTTCATGTGAAAGCAGACGTAAGTCA":
    observation_sequence_1.append(char)

transmission_probs_1 = {
    'Exon' : {'Exon' : 0.9, 'Donor' : 0.1, 'Intron' : 0.0},
    'Donor' : {'Exon' : 0.0, 'Donor' : 0.0, 'Intron' : 1.0},
    'Intron' : {'Exon' : 0.0, 'Donor' : 0.0, 'Intron' : 1.0}
}
emission_probs_1 = {
    'Exon' : {'A' :0.25 , 'C' :0.25, 'G' :0.25, 'T' :0.25},
    'Donor' : {'A' :0.05 , 'C' :0.00, 'G' :0.95, 'T' :0.00},
    'Intron' : {'A' :0.40 , 'C' :0.10, 'G' :0.10, 'T' :0.40}
}
start_probs_1 = {
    'Exon' : 1.0,
    'Donor' : 0.0,
    'Intron' : 0.0
}

def viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs):
    # So the goal is to find the optimal -- VITERBI PATH
    # We're gonna have a list(from index 0 to observation_sequence.length - 1) of dictionaries - for each state one dict
    # Since we have a trellis made up of |states| x observation_sequence.length. The dictionaries themselves consist of prob
    # and previous entries - probability being the max P(observation_sequence | state_sequence_so_far), and prev being the previous
    # state from which we would arrive to this/current state due to that transition having the HIGHEST PROBABILITY
    V = [{}]
    for st in states_set:
        V[0][st] = {"prob" : start_probs[st] * emission_probs[st][observation_sequence[0]], "prev" : None}

    for i in range(1, len(observation_sequence)):
        # Now for each state we have to compute the most likely probabilities and previous most likely state
        V.append({})

        for st in states_set:
            # Doing it for each state - need to compute the prob and prev
            # prev would be the state that has the MAXIMUM transmission probability to go into this/current state
            max_prev_prob = V[i-1][states_set[0]]["prob"] * transmission_probs[states_set[0]][st]
            max_prev_state = states_set[0]
            # let's find the previous state that has the highest probability to transmit into this/current state!
            for prev_st in states_set[1:]:
                prev_prob = V[i-1][prev_st]["prob"] * transmission_probs[prev_st][st]
                if prev_prob > max_prev_prob:
                    max_prev_prob = prev_prob
                    max_prev_state = prev_st

            # We've found the previous state that has the highest probability of transmitting into current state,
            # and we also have that probability value Hurray! Let's update the V list of dicts then
            V[i][st] = {"prob" : max_prev_prob * emission_probs[st][observation_sequence[i]], "prev" : max_prev_state}

    # The table has been filled. Let's print it
    for line in dptable(V):
        print(line)

    # Now it's time to traceback and print out the most likely sequence
    # Start from the last states, and find the one with the highest "prob" value

    # Viterbi decode/traceback part
    max_prob = V[-1][states_set[0]]["prob"]
    max_state = states_set[0]
    for st in states_set[1:]:
        curr_prob = V[-1][st]["prob"]
        curr_state = st
        if curr_prob > max_prob:
            max_prob = curr_prob
            max_state = curr_state

    viterbi_path = []
    viterbi_path.append(max_state)
    #Get all the previous states now
    curr_state = max_state
    for i in range(len(V) - 1, 0, -1):
        for st in states_set:
            if st == V[i][curr_state]["prev"]:
                curr_state = st
                viterbi_path.append(curr_state)
                break

    # Print the viterbi path in the reversed order
    viterbi_path.reverse()
    max_prob = max(V[-1][st]["prob"] for st in states_set)
    print('The most probable sequence of states is as follows: ' + ' '.join(viterbi_path)
          + ' with the highest probability being ' + str(max_prob))


def dptable(V):
    # Let's form the table that shall be printed out!
    # First line are the indexes observation sequence
    yield ' '.join('{:12d}'.format(i) for i in range(len(V)))
    for state in V[0]:
        yield '{:.7}'.format(state) + ': ' + ' '.join('{:.7f}'.format(v[state]["prob"]) for v in V)

print("Non-recursive")
viterbi(observation_sequence_1, states_set_1, transmission_probs_1, emission_probs_1, start_probs_1)
