# First set of values
observation_sequence = ('Normal', 'Cold', 'Dizzy')
states_set = ('Healthy', 'Fever')
start_probs = {
    'Healthy' : 0.6,
    'Fever' : 0.4
}
transmission_probs = {
    'Healthy' : {'Healthy' : 0.7, 'Fever' : 0.3},
    'Fever' : {'Healthy' : 0.4, 'Fever' : 0.6}
}
emission_probs = {
    'Healthy' : {'Normal' : 0.5, 'Cold' : 0.4, 'Dizzy' : 0.1},
    'Fever' : {'Normal' : 0.1, 'Cold': 0.3, 'Dizzy' : 0.6}
}

# Second set of values
states_set_1 = ('Exon', 'Donor', 'Intron')
observation_sequence_1 = []
for char in "CTTCATGTGAAAGCAGACGTAAGTCA":
    observation_sequence_1.append(char)

transmission_probs_1 = {
    'Exon' : {'Exon' : 0.9, 'Donor' : 0.1, 'Intron' : 0.0},
    'Donor' : {'Exon' : 0.0, 'Donor' : 0.0, 'Intron' : 1.0},
    'Intron' : {'Exon' : 0.0, 'Donor' : 0.0, 'Intron' : 1.0}
}
emission_probs_1 = {
    'Exon' : {'A' :0.25 , 'C' :0.25, 'G' :0.25, 'T' :0.25},
    'Donor' : {'A' :0.05 , 'C' :0.00, 'G' :0.95, 'T' :0.00},
    'Intron' : {'A' :0.40 , 'C' :0.10, 'G' :0.10, 'T' :0.40}
}
start_probs_1 = {
    'Exon' : 1.0,
    'Donor' : 0.0,
    'Intron' : 0.0
}


"""
Recursive function calculating viterbi path
"""
def viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, index_obs):
    if index_obs == 0:
        V = [{}]
        for st in states_set:
            prob = start_probs[st] * emission_probs[st][observation_sequence[0]]
            V[0][st] = {
                "prob" : prob,
                "prev" : None
            }

        return V

    index_prev = index_obs - 1
    V = viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, index_prev)

    # Do it for every state
    V.append({})
    for st in states_set:
        # Find the most probable transition from the previous/lower step
        state_prev_max = states_set[0]
        prob_prev_max = V[index_prev][state_prev_max]["prob"] * transmission_probs[state_prev_max][st]
        for state_prev_curr in states_set[1:]:
            prob_prev_curr = V[index_prev][state_prev_curr]["prob"] * transmission_probs[state_prev_curr][st]
            if prob_prev_curr > prob_prev_max:
                prob_prev_max = prob_prev_curr
                state_prev_max = state_prev_curr

        # Okay, now we have the highest probability and the state - return them back
        V[index_obs][st] = {
            "prob" : prob_prev_max * emission_probs[st][observation_sequence[index_obs]],
            "prev" : state_prev_max
        }


    #The V has been filled - now just return it for further filling and processing
    return V

def dptable(V):
    # Let's form the table that shall be printed out!
    # First line are the indexes observation sequence
    yield ' '.join('{:12d}'.format(i) for i in range(len(V)))
    for state in V[0]:
        yield '{:.7}'.format(state) + ': ' + ' '.join('{:.7f}'.format(v[state]["prob"]) for v in V)


def viterbi_decode(V, states_set):
    # The table has been filled. Let's print it
    for line in dptable(V):
        print(line)

    # Now it's time to traceback and print out the most likely sequence
    # Start from the last states, and find the one with the highest "prob" value

    # Viterbi decode/traceback part
    max_prob = V[-1][states_set[0]]["prob"]
    max_state = states_set[0]
    for st in states_set[1:]:
        curr_prob = V[-1][st]["prob"]
        curr_state = st
        if curr_prob > max_prob:
            max_prob = curr_prob
            max_state = curr_state

    viterbi_path = []
    viterbi_path.append(max_state)
    #Get all the previous states now
    curr_state = max_state
    for i in range(len(V) - 1, 0, -1):
        for st in states_set:
            if st == V[i][curr_state]["prev"]:
                curr_state = st
                viterbi_path.append(curr_state)
                break

    # Print the viterbi path in the reversed order
    viterbi_path.reverse()
    max_prob = max(V[-1][st]["prob"] for st in states_set)
    print('The most probable sequence of states is as follows: ' + ' '.join(viterbi_path)
          + ' with the highest probability being ' + str(max_prob))


print("Recursive")
last_index = len(observation_sequence)-1
V = viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, last_index)
# Now we have the filled table - this time done recursively!
viterbi_decode(V, states_set)


