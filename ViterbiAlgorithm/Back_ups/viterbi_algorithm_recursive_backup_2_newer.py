observation_sequence = ['Normal', 'Cold', 'Dizzy']
states_set = ('Healthy', 'Fever')
start_probs = {
'Healthy' : 0.6,
'Fever' : 0.4
}
transmission_probs = {
'Healthy' : {'Healthy' : 0.7, 'Fever' : 0.3},
'Fever' : {'Healthy' : 0.4, 'Fever' : 0.6}
}
emission_probs = {
'Healthy' : {'Normal' : 0.5, 'Cold' : 0.4, 'Dizzy' : 0.1},
'Fever' : {'Normal' : 0.1, 'Cold': 0.3, 'Dizzy' : 0.6}
}

"""
Recursive function calculating viterbi path
"""
def viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, index_obs, index_state):
    current_state = states_set[index_state]
    if index_obs == 0:
        return {
            "prob" : start_probs[current_state] * emission_probs[current_state][observation_sequence[0]],
            "prev" : [current_state]
            }

    index_next = index_obs - 1
    viterbi_data_max = viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, index_next, 0)
    prob_max = viterbi_data["prob"]
    for i in range(1, len(states_set)):
        # find the max of |q| - 1 recursions
        viterbi_data = viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, index_next, i)
        prob = viterbi_data["prob"]
        if prob > prob_max:
            viterbi_data_max = viterbi_data

    # We've found the max prob and the state whence it came from
    prev_prob = viterbi_data_max["prob"]
    prev_states = viterbi_data_max["prev"]
    prev_state_from = prev_states[-1]

    new_prob = prev_prob * transmission_probs[prev_state_from][current_state] * emission_probs[current_state][observation_sequence[index_obs]]
    new_states = prev_states.append(current_state)
    return {
        "prob" : new_prob,
        "prev" : new_states
    }


viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, )
