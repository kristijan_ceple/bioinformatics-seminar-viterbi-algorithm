observation_sequence = ['Normal', 'Cold', 'Dizzy']
states_set = ('Healthy', 'Fever')
start_probs = {
'Healthy' : 0.6,
'Fever' : 0.4
}
transmission_probs = {
'Healthy' : {'Healthy' : 0.7, 'Fever' : 0.3},
'Fever' : {'Healthy' : 0.4, 'Fever' : 0.6}
}
emission_probs = {
'Healthy' : {'Normal' : 0.5, 'Cold' : 0.4, 'Dizzy' : 0.1},
'Fever' : {'Normal' : 0.1, 'Cold': 0.3, 'Dizzy' : 0.6}
}

"""
Recursive function calculating viterbi path
"""
def viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, index_obs):
    if index_obs == 0:
        max_prev = states_set[0]
        max_prob = start_probs[max_prev] * emission_probs[max_prev][observation_sequence[index_obs]]

        for st in states_set[1:]:
            if start_probs[st] * emission_probs[st][observation_sequence[index_obs]] >  max_prob:
                max_prob = start_probs[st] * emission_probs[st][observation_sequence[index_obs]]
                max_prev = st

        return {"prob" : max_prob,
                "viterbi_path" : [max_prev]
                }

    viterbi_res = viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, index_obs-1)


viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, 5)
