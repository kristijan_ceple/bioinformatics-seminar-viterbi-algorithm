# First set of values
states_set = ('Healthy', 'Fever')
observation_sequence = ['Normal', 'Cold', 'Dizzy']
start_probs = {
    'Healthy' : 0.6,
    'Fever' : 0.4
}
transmission_probs = {
    'Healthy' : {'Healthy' : 0.7, 'Fever' : 0.3},
    'Fever' : {'Healthy' : 0.4, 'Fever' : 0.6}
}
emission_probs = {
    'Healthy' : {'Normal' : 0.5, 'Cold' : 0.4, 'Dizzy' : 0.1},
    'Fever' : {'Normal' : 0.1, 'Cold': 0.3, 'Dizzy' : 0.6}
}

# Second set of values
states_set_1 = ('Exon', 'Donor', 'Intron')
observation_sequence_1 = []
for char in "CTTCATGTGAAAGCAGACGTAAGTCA":
    observation_sequence_1.append(char)

transmission_probs_1 = {
    'Exon' : {'Exon' : 0.9, 'Donor' : 0.1, 'Intron' : 0.0},
    'Donor' : {'Exon' : 0.0, 'Donor' : 0.0, 'Intron' : 1.0},
    'Intron' : {'Exon' : 0.0, 'Donor' : 0.0, 'Intron' : 1.0}
}
emission_probs_1 = {
    'Exon' : {'A' :0.25 , 'C' :0.25, 'G' :0.25, 'T' :0.25},
    'Donor' : {'A' :0.05 , 'C' :0.00, 'G' :0.95, 'T' :0.00},
    'Intron' : {'A' :0.40 , 'C' :0.10, 'G' :0.10, 'T' :0.40}
}
start_probs_1 = {
    'Exon' : 1.0,
    'Donor' : 0.0,
    'Intron' : 0.0
}


"""
Recursive function calculating viterbi path
"""
def viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, index_obs):
    if index_obs == 0:
        start_state = states_set[0]
        prob_max = start_probs[start_state] * emission_probs[start_state][observation_sequence[0]]
        state_max = states_set[0]
        for st in states_set[1:]:
            prob = start_probs[st] * emission_probs[st][observation_sequence[0]]
            if prob > prob_max:
                prob_max = prob
                state_max = st

        return {
            "prob" : prob_max,
            "prev" : [state_max]
            }

    index_prev = index_obs - 1
    viterbi_data_max = viterbi(observation_sequence, states_set, transmission_probs, emission_probs, start_probs, index_prev)
    viterbi_data_prob_max = viterbi_data_max["prob"]
    prev_states = viterbi_data_max["prev"]
    prev_state_from = prev_states[-1]

    # Let's find the max for the current index - go through all the states
    state_max = states_set[0]
    prob_max = viterbi_data_prob_max * transmission_probs[prev_state_from][state_max] * emission_probs[state_max][observation_sequence[index_obs]]
    for st in states_set[1:]:
        prob = viterbi_data_prob_max * transmission_probs[prev_state_from][st] * emission_probs[st][observation_sequence[index_obs]]
        if prob > prob_max:
            prob_max = prob
            state_max = st

    # We've found the max prob and the state whence it came from
    prev_states.append(state_max)
    return {
        "prob" : prob_max,
        "prev" : prev_states
    }


viterbi_data_final = viterbi(observation_sequence_1, states_set_1, transmission_probs_1, emission_probs_1, start_probs_1, len(observation_sequence_1)-1)
max_prob = viterbi_data_final["prob"]
states_sequence = viterbi_data_final["prev"]
print('The Viterbi path is ' + ' '.join(states_sequence) + ' with the probability of ' + '{:f}'.format(max_prob))
