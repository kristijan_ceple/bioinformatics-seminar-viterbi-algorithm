https://ferhr-my.sharepoint.com/:p:/g/personal/kc50874_fer_hr/ETxrrVzELUxBvFPUN9AmLvEBJuSaU6GJ0ovUm8SgjJ7fwQ?e=7slIyN		-- The presentation
https://ferhr-my.sharepoint.com/:w:/g/personal/kc50874_fer_hr/EVSZzbmn5dBCvmvQ2jZt4YQB07FW-Jf2hC5WCtZN5pG9qw?e=fvETU5		-- The Word document

DISCLAIMER:
The codes provided with the seminar were written heavily using wikipedia due to
me learning python at the time and being focused on the written document. The
program was just meant to be a demonstration that would follow after the
presentation.

As such, it can't really be called my own as I've copied a lot of it.
https://en.wikipedia.org/wiki/Viterbi_algorithm
I did go through it, understand it thoroughly and comment it, but it wasn't
written fully independently and on my own.

It was my idea, however, to write separate recursive and non-recursive versions,
so I've rewritten the non-recursive wikipedia version into a recursive version.